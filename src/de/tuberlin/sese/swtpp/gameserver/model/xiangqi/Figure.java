package de.tuberlin.sese.swtpp.gameserver.model.xiangqi;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Figure extends Board implements Serializable{
	
	private static final long serialVersionUID = 5424778147226994452L;
	
	protected String boardFEN;
	protected char[][] boardArr;
	protected int playerColor;

	protected int startColumn;
	protected int startRow;
	protected char figureType;

	public Figure(String boardFEN, int playerColor) {
		this.boardFEN = boardFEN;
		this.boardArr = board2map(boardFEN);
		this.playerColor = playerColor;
	}

	public List<String> getPossibleMoves(String move) {
		int[] startPos = getStartTuple(move);
		this.startColumn = startPos[0];
		this.startRow = startPos[1];

		this.figureType = getFigureOnSpace(startPos);

		if (belongsToOpponent(startPos)) {
			return new LinkedList<>();
		}

		switch(Character.toLowerCase(this.figureType)) {
			case 'g':
				return getGeneralMoves();
			case 'a':
				return getAdvisorMoves();
			case 'e':
				return getElephantMoves();
			case 'h':
				return getHorseMoves();
			case 'r':
				return getRookMoves();
			case 'c':
				return getCannonMoves();
			case 's':
				return getSoldierMoves();
			default:
				return new LinkedList<>();
		}
	}

	public List<String> getGeneralMoves(){
		List<String> moves = new LinkedList<>();
		for (int i: new int[]{1, -1}) {
			int[] move = {startColumn, (startRow + i)};
			if (onBoard(move) && !belongsToMe(move) && isInPalace(move)){
				moves.add(convertToSpace(move));
			}
			move = new int[]{(startColumn + i), startRow};
			if (isInPalace(move) && !belongsToMe(move)){
				moves.add(convertToSpace(move));
			}
		}
		return moves;
	}

	public List<String> getAdvisorMoves(){
		List<String> moves = new LinkedList<>();

		ArrayList<int[]> potentialMoves = new ArrayList<>();

		// Up-Right Down-Left
		for (int i: new int[]{1, -1}) {
			int[] move = {(this.startColumn + i), (this.startRow + i)};
			potentialMoves.add(move);

			move = new int[]{(this.startColumn - i), (this.startRow + i)};
			potentialMoves.add(move);
		}
		for (int[] i : potentialMoves) {
            if (onBoard(i) && !belongsToMe(i) && isInPalace(i)) {
                moves.add(convertToSpace(i));
            }
		}
		return moves;
	}

	public List<String> getElephantMoves(){
		List<String> moves = new LinkedList<>();
		ArrayList<int[]> potentialMoves = new ArrayList<>();
		
		for (int i : new int[]{1, -1}) {
			// Up-Right and Down-Left
			int[] move = {(this.startColumn + (2 * i)), (this.startRow + (2 * i))};
			if (onBoard(new int[]{this.startColumn + i, this.startRow + i}) && getFigureOnSpace(new int[]{this.startColumn + i, this.startRow + i}) == '0') {
				potentialMoves.add(move);
			}
			// Up-Left and Down-Right
			move = new int[]{(this.startColumn - (2 * i)), (this.startRow + (2 * i))};
			if (onBoard(new int[]{this.startColumn - i, this.startRow + i}) && getFigureOnSpace(new int[]{this.startColumn - i, this.startRow + i}) == '0') {
				potentialMoves.add(move);
			}
		}

		// Iterate through potential moves
		for (int[] i : potentialMoves) {
			// If river not crossed
			if (onBoard(i) && !belongsToMe(i) && !overRiver(i[1])) {
				moves.add(convertToSpace(i));
			}
		}
		return moves;
	}

	public List<String> getHorseMoves(){
		List<String> moves = new LinkedList<>();
		for (int i: new int[]{1,-1}) {
			for (int j: new int[]{1,-1}) {
				int[] firstMove = {(startColumn + j), startRow};
				int[] secondMove = {(startColumn + (2*j)), (startRow + i)};
				if (onBoard(secondMove) && isFree(firstMove) && !belongsToMe(secondMove)){//up/down
					moves.add(convertToSpace(secondMove));
				}
				firstMove = new int[]{startColumn, (startRow + j)};
				secondMove = new int[]{(startColumn + i), (startRow + (2*j))};
				if (onBoard(secondMove) && isFree(firstMove) && !belongsToMe(secondMove)){//right/left
					moves.add(convertToSpace(secondMove));
				}
			}
		}
		return moves;
	}

	public List<String> getRookMoves(){
		List<String> moves = new LinkedList<>();
		for (int i = -10; i < 10; i++) {
			int[] move = new int[]{(startColumn + i), startRow};
            if (onBoard(move) && !belongsToMe(move) && occupiedSpacesHorizontal(move, startColumn) == 0){//vertical
                moves.add(convertToSpace(move));
            }

			move = new int[]{startColumn, (startRow + i)};
            if (onBoard(move) && !belongsToMe(move) && occupiedSpacesVertical(move, startRow) == 0){//horizontal
                moves.add(convertToSpace(move));
            }
		}
		return moves;
	}

	public List<String> getCannonMoves(){
		List<String> moves = new LinkedList<>();
		moves.addAll(getCannonMovesVertical());
		moves.addAll(getCannonMovesHorizontal());
		return moves;
	}

	public List<String> getCannonMovesHorizontal(){
		List<String> moves = new LinkedList<>();
		for (int i = -8; i < 9; i++) {//horizontal
			int[] move = {(startColumn + i), startRow};
			int occupiedSpaces = occupiedSpacesHorizontal(move, startColumn);
			if (onBoard(move) && ((isFree(move) && occupiedSpaces == 0) || (belongsToOpponent(move) && occupiedSpaces == 1))){
				moves.add(convertToSpace(move));
			}
		}
		return moves;
	}

	public List<String> getCannonMovesVertical(){
		List<String> moves = new LinkedList<>();
		for (int i = -9; i < 10; i++) {//vertical
			int[] move = {startColumn, (startRow + i)};
			int occupiedSpaces = occupiedSpacesVertical(move, startRow);
			if (onBoard(move) && ((isFree(move) && occupiedSpaces == 0) || (belongsToOpponent(move) && occupiedSpaces == 1))){
				moves.add(convertToSpace(move));
			}
		}
		return moves;
	}

	public List<String> getSoldierMoves(){
		List<String> moves = new LinkedList<>();

		ArrayList<int[]> potentialMoves = new ArrayList<>();

		// Vertical move and check if inside board
		int[] move = {this.startColumn, (this.startRow + this.playerColor)};
		potentialMoves.add(move);

		// Horizontal move right and left
		for (int i : new int[]{1, -1}) {
			move = new int[]{(this.startColumn + i), this.startRow};
			potentialMoves.add(move);
		}

		// Check color of figure and check if river passed
		for (int[] i : potentialMoves) {
			if (onBoard(i) && !belongsToMe(i) && (i[0] == this.startColumn || overRiver(this.startRow))) {
				moves.add(convertToSpace(i));
			}
		}
		return moves;
	}

	/*-------------------
	 * HELPER METHODS
	 * ------------------
	 */

	// Check if indexes inside of board

	// Check if indexes over river
	public boolean overRiver(int row) {
        return (this.playerColor == 1 && row >= 5) || (this.playerColor == -1 && row <= 4);
    }

	// Get character on space in board
	public char getFigureOnSpace(int[] pos){
        return this.boardArr[pos[0]][pos[1]];
	}

	public boolean isFree(int[] pos){
		return (boardArr[pos[0]][pos[1]] == '0');
	}

	public boolean belongsToMe(int[] pos){
		if (this.boardArr[pos[0]][pos[1]] == '0'){
			return false;
		}else if (this.playerColor == 1){//red
			return Character.isUpperCase(this.boardArr[pos[0]][pos[1]]);
		}else {//black
			return Character.isLowerCase(this.boardArr[pos[0]][pos[1]]);
		}
	}

	public boolean belongsToOpponent(int[] pos){
		if (boardArr[pos[0]][pos[1]] == '0'){
			return false;
		}else if (this.playerColor == -1){//red
			return Character.isUpperCase(boardArr[pos[0]][pos[1]]);
		}else {//black
			return Character.isLowerCase(boardArr[pos[0]][pos[1]]);
		}
	}

	public boolean isInPalace(int[] pos){
		if (playerColor == 1){
			return (pos[0] >= 3 && pos[0] <= 5 && pos[1] <= 2);
		}else {
			return (pos[0] >= 3 && pos[0] <= 5 && pos[1] >= 7);
		}
	}

	public int occupiedSpacesHorizontal(int[] pos, int column){
		int occupiedSpaces = 0;
		for (int i = Math.min(pos[0], column) + 1; i < Math.max(pos[0], column); i++) {
			if (onBoard(new int[]{i, pos[1]}) && this.boardArr[i][pos[1]] != '0'){
				occupiedSpaces++;
			}
		}
		return occupiedSpaces;
	}

	public int occupiedSpacesVertical(int[] pos, int row){
		int occupiedSpaces = 0;
		for (int i = Math.min(pos[1], row) + 1; i < Math.max(pos[1], row); i++) {
			if (onBoard(new int[]{pos[0], i}) && this.boardArr[pos[0]][i] != '0'){
				occupiedSpaces++;
			}
		}
		return occupiedSpaces;
	}

}