package de.tuberlin.sese.swtpp.gameserver.model.xiangqi;

import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class Board implements Serializable{

	private static final long serialVersionUID = 5424778147226994452L;

	/*-------------------
	 * BOARD METHODS
	 * ------------------
	 */

	public static char[][] board2map(String boardFEN){
		StringBuilder extended = new StringBuilder();
		char[][] board = new char[9][10];

		for(char c: boardFEN.toCharArray()){
			if(Character.isDigit(c)){
				extended.append(new String(new char[Character.getNumericValue(c)]).replace("\0", "0"));
			} else {
				extended.append(c);
			}
		}

		String[] arrOfRows = extended.toString().split("/", 10);
		for(int i = 0; i < 9; i++){
			for(int j = 0; j < 10; j++){
				board[i][j] = arrOfRows[9-j].charAt(i);
			}
		}

		return board;
	}

	// Conversion from 2D Array to FEN
	public static String map2board(char[][] map){
		StringBuilder board = new StringBuilder();
		for (int i = 0; i < 10; i++) {
			StringBuilder row = new StringBuilder();
			for (int count = 0, j = 0; j < 9; j++) {
				if (count > 0 && map[j][i] != '0'){
					row.append(count);
					count = 0;
				}
				if (map[j][i] != '0'){
					row.append(map[j][i]);
					continue;
				}
				count++;
				if (j == 8){
					row.append(count);
				}
			}
			board.insert(0, row + "/");
		}
		board = new StringBuilder(board.substring(0, board.length() - 1));
		return board.toString();
	}

	public String doMove(String boardFEN, String move) {
		int[] start = getStartTuple(move);
		int[] dest = getDestTuple(move);
		char[][] boardArr = board2map(boardFEN);
		boardArr[dest[0]][dest[1]] = boardArr[start[0]][start[1]];
		boardArr[start[0]][start[1]] = '0';
		return map2board(boardArr);
	}

	/*-------------------
	 * CONVERSION METHODS
	 * ------------------
	 */

	public String convertToSpace(int[] pos) {
		int column = pos[0];
		int row = pos[1];

		String space = "";
		space += (char) (column + 'a');
		space += (char) (row + '0');
		return space;
	}

	public int[] getStartTuple(String move) {
		int[] moveArray = new int[2];
		// Get start column
		moveArray[0] = move.charAt(0) - 'a';
		// Get start row
		moveArray[1] = move.charAt(1) - '0';
		return moveArray;
	}

	public int[] getDestTuple(String move) {
		int[] moveArray = new int[2];
		// Get start column
		moveArray[0] = move.charAt(3) - 'a';
		// Get start row
		moveArray[1] = move.charAt(4) - '0';
		return moveArray;
	}

	/*-------------------
	 * HELPER METHODS
	 * ------------------
	 */

	public List<String> getAllPossibleMoves(int player, String boardFEN){
		List<String> allPossibleMoves = new LinkedList<>();

		Figure figure = new Figure(boardFEN, player);
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 10; j++) {
				int[] start = new int[]{i,j};
				List<String> currentMoves = figure.getPossibleMoves(figure.convertToSpace(start));

                for (String currentMove : currentMoves) {
                    String move = convertToSpace(start) + "-" + currentMove;
                    allPossibleMoves.add(move);
                }
			}
		}
		return allPossibleMoves;
	}

	public boolean noPossibleMoves(int player,String boardFEN){
		List<String> allPossibleMoves = getAllPossibleMoves(player, boardFEN);

        for (String allPossibleMove : allPossibleMoves) {
            String tempBoard = doMove(boardFEN, allPossibleMove);
            if (!isChecked(player, tempBoard) && !isDeathStare(tempBoard)) {
                return false;
            }
        }
		return true;
	}

	public boolean isChecked(int player, String boardFEN){
		Set<String> checkedSpaces = new HashSet<>();
		for (String s : getAllPossibleMoves(player*(-1), boardFEN)) {
			checkedSpaces.add(convertToSpace(getDestTuple(s)));
		}
		String generalPos = getGeneralPos(player, boardFEN);
		return checkedSpaces.contains(generalPos);
	}

	public String getGeneralPos(int player, String boardFEN){
		char[][] boardArr = board2map(boardFEN);
		int column = 0;
		int row = 0;
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 10; j++) {
				if ((player == 1 && boardArr[i][j] == 'G') || (player == -1 && boardArr[i][j] == 'g')){
					column = i;
					row = j;
					break;
				}
			}
		}
		return convertToSpace(new int[]{column,row});

	}

	public boolean isDeathStare(String boardFEN){
		int[] redGeneralPos = getStartTuple(getGeneralPos(1, boardFEN));
		int[] blackGeneralPos = getStartTuple(getGeneralPos(-1, boardFEN));
		if (redGeneralPos[0] != blackGeneralPos[0]) {
			return false;
		}
		int occupiedSpaces = 0;
		char[][] boardArr = board2map(boardFEN);
		for (int i = redGeneralPos[1] + 1; i < blackGeneralPos[1]; i++) {
			if (boardArr[redGeneralPos[0]][i] != '0'){
				occupiedSpaces++;
			}
		}
		return (occupiedSpaces == 0);
	}

	public boolean onBoard(int[] pos) {
		int column = pos[0];
		int row = pos[1];

        return column <= 8 && column >= 0 && row <= 9 && row >= 0;
    }

}