package de.tuberlin.sese.swtpp.gameserver.test.xiangqi;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import de.tuberlin.sese.swtpp.gameserver.control.GameController;
import de.tuberlin.sese.swtpp.gameserver.model.Game;
import de.tuberlin.sese.swtpp.gameserver.model.Player;
import de.tuberlin.sese.swtpp.gameserver.model.User;

public class TryMoveIntegrationTest {


	User user1 = new User("Alice", "alice");
	User user2 = new User("Bob", "bob");
	
	Player redPlayer = null;
	Player blackPlayer = null;
	Game game = null;
	GameController controller;
	
	@Before
	public void setUp() throws Exception {
		controller = GameController.getInstance();
		controller.clear();
		
		int gameID = controller.startGame(user1, "", "xiangqi");
		
		game =  controller.getGame(gameID);
		redPlayer = game.getPlayer(user1);

	}
	
	public void startGame() {
		controller.joinGame(user2, "xiangqi");		
		blackPlayer = game.getPlayer(user2);
	}
	
	public void startGame(String initialBoard, boolean redNext) {
		startGame();
		
		game.setBoard(initialBoard);
		game.setNextPlayer(redNext? redPlayer:blackPlayer);
	}
	
	public void assertMove(String move, boolean red, boolean expectedResult) {
		if (red)
			assertEquals(expectedResult, game.tryMove(move, redPlayer));
		else 
			assertEquals(expectedResult,game.tryMove(move, blackPlayer));
	}
	
	public void assertGameState(String expectedBoard, boolean redNext, boolean finished, boolean redWon) {
		assertEquals(expectedBoard,game.getBoard());
		assertEquals(finished, game.isFinished());

		if (!game.isFinished()) {
			assertEquals(redNext, game.getNextPlayer() == redPlayer);
		} else {
			assertEquals(redWon, redPlayer.isWinner());
			assertEquals(!redWon, blackPlayer.isWinner());
		}
	}
	

	/*******************************************
	 * !!!!!!!!! To be implemented !!!!!!!!!!!!
	 *******************************************/
	
	@Test
	public void exampleTest() {
	    startGame("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/9/RHEAGAEHR",true);
	    assertMove("e3-e4",true,true);
	    assertGameState("rheagaehr/9/1c5c1/s1s1s1s1s/9/4S4/S1S3S1S/1C5C1/9/RHEAGAEHR",false,false,false);
	}
	
	@Test
	public void playerSwitchTest() {
		// Player Switch
		startGame("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/9/RHEAGAEHR",false);
		assertMove("e6-e5",false,true);
		assertGameState("rheagaehr/9/1c5c1/s1s3s1s/4s4/9/S1S1S1S1S/1C5C1/9/RHEAGAEHR",true,false,false);

        startGame("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/9/RHEAGAEHR",true);
        assertMove("e3-e4",true,true);
        assertGameState("rheagaehr/9/1c5c1/s1s1s1s1s/9/4S4/S1S3S1S/1C5C1/9/RHEAGAEHR",false,false,false);
    }
	
	@Test
	public void invalidMovesTest() {
		// Invalid Move
		startGame("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/9/RHEAGAEHR",true);
		assertMove("e3-e2",true,false);
		assertGameState("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/9/RHEAGAEHR",true,false,false);
	}
	
	@Test
	public void checkMateTest() {
		// Check mate
		startGame("3g5/9/3R5/3R5/9/9/S1S1S1S1S/1C5C1/9/RHEAGAEHR",true);
		assertMove("d7-e7",true,true);
		assertGameState("3g5/9/4R4/3R5/9/9/S1S1S1S1S/1C5C1/9/RHEAGAEHR",false,true,true);
		
		// Check mate with deathStare
		startGame("3g5/9/9/9/9/9/9/9/2R6/1HEAGAEH1", true);
		assertMove("c1-d1",true,true);
		assertGameState("3g5/9/9/9/9/9/9/9/3R5/1HEAGAEH1",false,true,true);
	}
	
	@Test
	public void wrongPlayerTest() {
		startGame("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/9/RHEAGAEHR",false);
		assertMove("e3-e2",true,false);
		assertGameState("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/9/RHEAGAEHR",false,false,false);
	}
	
	@Test
	public void invalidMoveStringTest() {
		startGame("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/9/RHEAGAEHR",true);
		assertMove("3a-2t",true,false);
		assertGameState("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/9/RHEAGAEHR",true,false,false);
	}
	
	
	@Test
	public void wrongMoveTest() {
		// Wrong move Check
		startGame("rheagaehr/4R4/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/9/1HEAGAEHR",false);
		assertMove("a9-a8",false,false);
		assertGameState("rheagaehr/4R4/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/9/1HEAGAEHR",false,false,false);
		
		// Wrong move deathstare
		startGame("rheagaehr/9/1c5c1/s1s3s1s/9/9/S1S3S1S/1C5C1/9/RHEAGAEHR",true);
		assertMove("a0-a1",true,false);
		assertGameState("rheagaehr/9/1c5c1/s1s3s1s/9/9/S1S3S1S/1C5C1/9/RHEAGAEHR",true,false,false);
	}
	
	@Test
	public void overRiverTest() {
		// Move as black not over river
		startGame("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/9/RHEAGAEHR",false);
	    assertMove("e6-e5",false,true);
	    assertGameState("rheagaehr/9/1c5c1/s1s3s1s/4s4/9/S1S1S1S1S/1C5C1/9/RHEAGAEHR",true,false,false);
	    // Move as red over river
	    startGame("rheagaehr/9/1c5c1/s1s1s1s1s/4S4/9/S1S3S1S/1C5C1/9/RHEAGAEHR",true);
	    assertMove("e5-e6",true,true);
	    assertGameState("rheagaehr/9/1c5c1/s1s1S1s1s/9/9/S1S3S1S/1C5C1/9/RHEAGAEHR",false,false,false);
	    // Move as black over river
	    startGame("rheagaehr/9/1c5c1/s1s1s1s1s/9/4s4/S1S1S1S1S/1C5C1/9/RHEAGAEHR",false);
	    assertMove("e4-e3",false,true);
	    assertGameState("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1s1S1S/1C5C1/9/RHEAGAEHR",true,false,false);
	}
	
	@Test
	public void inPalaceTest() {
		// Red out of palace
		startGame("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/9/RHEAGAEHR",true);
	    assertMove("d0-c1",true,false);
	    assertGameState("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/9/RHEAGAEHR",true,false,false);
	}
	
	@Test
	public void generalPosTest() {
		startGame("rhea1aehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/9/RHEA1AEHR",true);
	    assertMove("e3-e4",true,false);
	    assertGameState("rhea1aehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/9/RHEA1AEHR",true,false,false);
	}
	
	@Test
	public void noFigureTest() {
		startGame("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/9/RHEAGAEHR",true);
	    assertMove("d3-d4",true,false);
	    assertGameState("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/9/RHEAGAEHR",true,false,false);
	}
	
	@Test
	public void spaceFreeTest() {
		startGame("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/SCS1S1S1S/7C1/9/RHEAGAEHR",true);
	    assertMove("b3-c3",true,false);
	    assertGameState("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/SCS1S1S1S/7C1/9/RHEAGAEHR",true,false,false);
	}
	
	
	/**************************
	 * 
	 * FIGURE TESTS
	 * 
	 **************************/
	
	@Test
	public void elephantMovesTest() {		
		// Figure in way top right
		startGame("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/7C1/3C5/RHEAGAEHR",true);
	    assertMove("c0-e2",true,false);
	    assertGameState("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/7C1/3C5/RHEAGAEHR",true,false,false);
	    
	    // Figure in way top left
	    startGame("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/7C1/1C7/RHEAGAEHR",true);
	    assertMove("c0-a2",true,false);
	    assertGameState("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/7C1/1C7/RHEAGAEHR",true,false,false);
	    
	    // Over river
	    startGame("rheagaehr/9/1c5c1/s1s1s1s1s/9/6E2/S1S1S1S1S/1C5C1/9/RH1AGAEHR",true);
	    assertMove("g4-i6",true,false);
	    assertGameState("rheagaehr/9/1c5c1/s1s1s1s1s/9/6E2/S1S1S1S1S/1C5C1/9/RH1AGAEHR",true,false,false);
	    
	    // Over river with ally figure
	    startGame("rheagaehr/9/1c5c1/s1s1s1s1S/9/6E2/S1S1S1S2/1C5C1/9/RH1AGAEHR",true);
	    assertMove("g4-i6",true,false);
	    assertGameState("rheagaehr/9/1c5c1/s1s1s1s1S/9/6E2/S1S1S1S2/1C5C1/9/RH1AGAEHR",true,false,false);
	    
	    // Not on board
	    startGame("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/7E1/RHEAGA1HR",true);
	    assertMove("h1-f3",true,true);
	    assertGameState("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1SES1S/1C5C1/9/RHEAGA1HR",false,false,false);
	}
	
	@Test
	public void advisorMovesTest() {
		// Occupied space
		startGame("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/4G4/RHEA1AEHR",true);
	    assertMove("d0-e1",true,false);
	    assertGameState("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/4G4/RHEA1AEHR",true,false,false);
	}
	
	@Test
	public void generalMovesTest() {
		// Out of palace
		startGame("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/3G5/RHEA1AEHR",true);
		assertMove("d1-c1",true,false);
		assertGameState("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/3G5/RHEA1AEHR",true,false,false);
		
		startGame("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C2G2C1/9/RHEA1AEHR",true);
	    assertMove("e3-e4",true,true);
	    assertGameState("rheagaehr/9/1c5c1/s1s1s1s1s/9/4S4/S1S3S1S/1C2G2C1/9/RHEA1AEHR",false,false,false);
	    
	    startGame("rhea1aehr/9/1c2g2c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/9/RHEAGAEHR",false);
	    assertMove("e6-e5",false,true);
	    assertGameState("rhea1aehr/9/1c2g2c1/s1s3s1s/4s4/9/S1S1S1S1S/1C5C1/9/RHEAGAEHR",true,false,false);
		
		// Out of board
		startGame("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/8G/RHEA1AEHR",true);
	    assertMove("i1-j1",true,false);
	    assertGameState("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/8G/RHEA1AEHR",true,false,false);
	
	    
	}
	
	@Test
	public void cannonMovesTest() {
		// Horizontal Cannon Moves
		startGame("rheagaehr/9/1c5c1/sCs1s1s1s/9/9/S1S1S1S1S/7C1/9/RHEAGAEHR",true);
	    assertMove("b6-e6",true,true);
	    assertGameState("rheagaehr/9/1c5c1/s1s1C1s1s/9/9/S1S1S1S1S/7C1/9/RHEAGAEHR",false,false,false);
	}
	
	@Test
	public void horseMovesTest() {
		// Occupied first move vertical
		startGame("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/1R7/1HEAGAEHR",true);
	    assertMove("b0-a2",true,false);
	    assertGameState("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/1R7/1HEAGAEHR",true,false,false);
		
		// Occupied second move vertical
		startGame("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/RC5C1/9/1HEAGAEHR",true);
	    assertMove("b0-a2",true,false);
	    assertGameState("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/RC5C1/9/1HEAGAEHR",true,false,false);
	    
	    // Occupied second move horizontal
	    startGame("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/3G5/RH1AGAEHR",true);
	    assertMove("b0-d1",true,false);
	    assertGameState("rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/3G5/RH1AGAEHR",true,false,false);
	}
	//TODO: implement test cases of same kind as example here

}
